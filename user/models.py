from django.db import models

# Create your models here.

class Info(models.Model):
	name = models.CharField(max_length=30)
	number = models.IntegerField()
	address = models.TextField()
