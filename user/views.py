from django.shortcuts import render
from django.http import HttpResponse
from django import forms
from .models import Info
from .forms import PostForm,InsertionForm
from django.shortcuts import redirect
from django.views import View
# Create your views here.

def check(request):
	if request.method == "POST":
		form = PostForm(request.POST)
		if form.is_valid():
			name = form.cleaned_data['name']
			number = form.cleaned_data['number']
			address = form.cleaned_data['address']
			print(name)
			return redirect('home')
	else:
		form = PostForm()
		return render(request,"index.html",{'form':form})

def insertion(request):
	if request.method == "POST":
		form = InsertionForm(request.POST)
		if form.is_valid():
			print("in")
			form.save()
			return redirect('home')
	else:
		return redirect('home')

class NewClass(View):
	greeting = "Good Day"
	def get(self,request):
		return HttpResponse("in")
	def post(self,request):
		return HttpResponse(self.greeting)
	def fun(self,request):
		return HttpResponse("own funtion")


