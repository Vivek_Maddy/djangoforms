from django import forms
from .models import Info

class PostForm(forms.Form):
	name = forms.CharField(label="Name")
	number = forms.IntegerField(label="Number")
	address = forms.CharField(label="Address")

class InsertionForm(forms.ModelForm):
    class Meta:
        model = Info
        fields = ('name','number','address',)