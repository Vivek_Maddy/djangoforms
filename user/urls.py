from django.urls import path
from .import views
from user.views import NewClass

urlpatterns = [
    path('', views.check,name="home"),
    path('insert/', views.insertion,name="insert"),
    path('classview/',NewClass.as_view()),
    # path('custom/',NewClass.fun(self,request)),


]
